Page({
    onLoad: function (options) {
        var that = this;


        wx.showToast({
            title: 'Loading...',
            icon: 'loading',
            duration: 2000
        });

        wx.request({
            url: 'https://mini-app.ffshtest.net/wechat/sign', //仅为示例，并非真实的接口地址
            data: {
                name: options.title,
            },
            header: {
                'Content-Type': 'application/json'
            },
            success: function(res) {

                let signData = JSON.parse(res.data);

                that.setData({
                    sign: JSON.parse(res.data)
                });

                wx.setNavigationBarTitle({
                    title: '今日运势'
                });

                wx.hideToast();
            }
        })
    }
});