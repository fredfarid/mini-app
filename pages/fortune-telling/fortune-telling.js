var app = getApp();

Page({
    data: {
        messages: [{to: false, text: 'Hi, I am Master Hu.Here u can ask me anything about your future.'}],
        userInfo: app.globalData.userInfo,
        input: ''
    },
    onLoad: function (options) {

        let that = this;

        app.getUserInfo(function (userInfo) {

            that.setData({
                userInfo: userInfo
            });

            wx.connectSocket({
                url: 'wss://mini-app.ffshtest.net',
                data: {
                    userInfo: 'hhaha'
                },
                fail: function (e) {
                    console.log('fail: ' + e)
                },
                success: function (e) {
                    console.log('success: ' + e)
                }
            });

            wx.onSocketOpen(function (res) {

                // wx.sendSocketMessage({
                //
                //     data: JSON.stringify({
                //         nickName: userInfo.nickName
                //     })
                // })
            });

            wx.onSocketMessage(function (res) {

                let receivedData = JSON.parse(res.data);
                console.log('收到服务器内容：' + JSON.stringify(receivedData), receivedData);
                if (!receivedData.text) {
                    return;
                }
                let messages = that.data.messages;
                messages.push(receivedData);
                that.setData({
                    messages: messages
                });
            });

            wx.onSocketClose(function (res) {

                console.log('WebSocket 已关闭！')
            });
        });

    },
    onReady: function () {

        wx.setNavigationBarTitle({
            title: 'Fortune-telling'
        });
    },
    recordInput: function (e) {

        this.setData({
            input: e.detail.value
        });
    },

    sendMsg: function () {

        if (!this.data.input) {
            return;
        }

        let data = {
            nickName: this.data.userInfo.nickName,
            messager: this.data.userInfo.avatarUrl,
            text: this.data.input,
            to: true
        };
        if (data.nickName === '云山雾罩') {
            data.to = false;
        }
        // let messages = this.data.messages;
        // messages.push(data);
        // this.setData({
        //     messages: messages
        // });
        console.log(this.data.messages);
        wx.sendSocketMessage({
            data: JSON.stringify(data)
        })
    },
    scrollToBottom: function () {
        console.log('dadadad')
    }
});

