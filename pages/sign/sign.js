var moment = require('../../library/moment.min');

Page({
    data: {
        defaultDate: moment(),
        startDate: '1900-09-01',
        endDate: '2020-09-01',
        date: ''
    },
    bindDateChange: function (e) {

        this.setData({
            date: e.detail.value
        })
    },
    onload: function (e) {

    },
    onReady: function () {
        wx.setNavigationBarTitle({
            title: 'Sign'
        });
    }
});
