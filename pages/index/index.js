var app = getApp();

var pageObject = {
    data: {
        userInfo: {},
        image: {},
        address:{},
        systemInfo:{},
        markers: [{
            latitude: 31.245474,
            longitude: 121.493729,
            name: 'I am here',
            desc: 'Hello World!'
        }],
        covers: [{
            latitude: 31.245475,
            longitude: 121.493729,
            iconPath: '../../images/face1.png',
            rotate: -10
        }, {
            latitude: 31.245476,
            longitude: 121.493729,
            iconPath: '../../images/face2.png',
            rotate: 0
        },
            {
                latitude: 31.245477,
                longitude: 121.493729,
                iconPath: '../../images/face3.png',
                rotate: 10
            }],
    },

    takePhoto: function (e) {

        wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                var tempFilePaths = res.tempFilePaths[0];
                console.log(tempFilePaths);
                wx.getImageInfo({
                    src: tempFilePaths,
                    success: function (res) {
                        wx.setAppData({
                            image: {
                                src: tempFilePaths,
                                width: res.width,
                                height: res.height
                            }
                        })
                    }
                })

            }

        })
    },

    showToast: function (e) {

        wx.showToast({
            title: 'Success',
            icon: 'success',
            duration: 1000
        })
    },

    chooseLocation: function (e) {

        wx.chooseLocation({
            success: function (res) {
                console.log(wx, this);
                wx.setAppData({
                    address: {
                        name: res.name,
                        address: res.address,
                        latitude: res.latitude,
                        longitude: res.longitude
                    }
                })
            }
        });
    },

    bindViewTap: function () {
        wx.navigateTo({
            url: '../logs/logs'
        })
    },

    onLoad: function () {

        var that = this;
        //调用应用实例的方法获取全局数据
        app.getUserInfo(function (userInfo) {
            //更新数据
            that.setData({
                userInfo: userInfo
            });
        });

        wx.getSystemInfo({
            success: function (res) {
                // console.log(res.model);
                // console.log(res.pixelRatio);
                // console.log(res.windowWidth);
                // console.log(res.windowHeight);
                // console.log(res.language);
                // console.log(res.version);
                that.setData({systemInfo:{
                    model:res.model,
                    version:res.version,
                    language:res.language
                }})
            }
        })
    }
};

Page(pageObject);